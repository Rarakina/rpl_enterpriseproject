﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project.Models
{
    class OrdersDetailModel
    {
        public class OrdersDetailModel : OrdersDetail
        {
            public OrdersDetailModel Entity(dynamic result)
            {
                var entity = new OrdersDetailModel()
                {
                    Id = result["Id"].ToString() as string,
                    Orders = {
                    Id = result["Orders"]["Id"].ToString() as string,
                    CurrDate = result["Orders"]["CurrDate"].ToString() as string,
                    Customer = result["Orders"]["Customer"].ToString() as string,
                    Total = result["Orders"]["Total"].ToString() as string,
                },
                    Qty = result["Qty"].ToString() as string,
                    Amount = result["Amount"].ToString() as string,
                };
                return entity;
            }
        }
    }
}
