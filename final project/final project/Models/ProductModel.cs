﻿using final_project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final_project.Models
{
    class ProductModel
    public class ProductModel : Product
    {
        public ProductModel Entity(dynamic result)
        {
            var entity = new ProductModel()
            {
                Id = result["Id"].ToString() as string,
                Name = result["Name"].ToString() as string,
                Price = result["Price"].ToString() as string,
                Guarantee = result["Guarantee"].ToString() as string,
            };
            return entity;
        }
    }
}

